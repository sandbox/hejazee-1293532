<?php
// $Id $

/**
 * @author "Ahmad Hesamzadeh" <mngafa@gmail.com>.
 *
 * @file
 *   Codes for administering staticbutton settings
 */

/**
 * Form builder. Configure staticbutton
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function staticbutton_admin_settings() {
  $module_path = base_path() . drupal_get_path('module', 'staticbutton');
  $default_image_path = $module_path . '/button.png';
  $default_image_link = 'admin/settings/staticbutton';
  
  $form['staticbutton_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable or disable the Static Button.'),
    '#description' => t('If you select Enabled, the Static Button will be shown to the user in selected pages.'),
    '#options' => array(
      t('Enabled'),  //0
      t('Disabled'), //1
    ),
    '#default_value' => variable_get('staticbutton_enabled', 0),
  );
  
  $form['staticbutton_image_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Static Button image path'),
    '#description' => t('Enter a relative path to an image that will be used as the static button image. This path must be relative to site root.'),
    '#default_value' => variable_get('staticbutton_image_path', $default_image_path),
  );
  
  $form['staticbutton_image_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Static Button link path'),
    '#description' => t('Enter a path that the static button will be linked to it. This can be an internal Drupal path such as %internal-path or an external URL such as %external-url. Enter %front to link to the front page.', array('%internal-path' => 'blog', '%external-url' => 'http://drupal.org/', '%front' => '<front>')),
    '#default_value' => variable_get('staticbutton_image_link', $default_image_link),
  );
  
  $access = user_access('use PHP for block visibility');
  $options = array(t('Show on every page except the listed pages.'), t('Show on only the listed pages.'));
  $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  
  if ($access) {
    $options[] = t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
    $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
  }
  $form['staticbutton_visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show Static Button on specific pages'),
    '#options' => $options,
    '#default_value' => variable_get('staticbutton_visibility', 0),
  );
  $form['staticbutton_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('staticbutton_pages', ''),
    '#description' => $description,
  );
  
  return system_settings_form($form);
}