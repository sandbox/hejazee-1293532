<?php
/**
 * The following variables are available:
 * - $imagelink :
 *     The url of the target page. When the user clicks on the button,
 *     the browser should go to this page.
 * - $imagepath :
 *     The path of the image for use in the static button.
 */
?>
<div id="staticbutton">
  <a href="<?php print $imagelink; ?>" rel="nofollow">
    <img src="<?php print $imagepath; ?>" alt="" />
  </a>
</div>